﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;        // 카메라가 추적할 대상
    public float smooting = 5f;     // 카메라가 지연되는 정도

    Vector3 offset;                 // 카메라와 대상의 거리

    private void Start()
    {
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;    // 이동할 위치
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smooting * Time.deltaTime);
    }
}
