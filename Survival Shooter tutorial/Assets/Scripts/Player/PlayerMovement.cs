﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;            // 플레이어 속도
    public JoyStickMove joyStick;
    public JoyStickCameraMove joyStickCamera;

    private Vector3 movement;           // 
    private Animator anim;              //
    private Rigidbody playerRigidbody;  //
    private Transform playerTr;         //
    private int floorMask;              // 바닥
    private float camRayLength = 100f;  // 

    private float h = 0f;
    private float v = 0f;
    private float ch = 0f;

    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        playerTr = GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        //float h = Input.GetAxisRaw("Horizontal");
        //float v = Input.GetAxisRaw("Vertical");

        h = joyStick.Horizontal();
        v = joyStick.Vertical();
        ch = joyStickCamera.Horizontal();

        Move(h, v);
        Turning(ch);
        Animating(h, v);
        //Debug.Log("H: " + h.ToString() + "  V: " + v.ToString() + "  CH: " + ch.ToString());
    }

    private void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    private void Turning(float ch)
    {
        //Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //RaycastHit floorHit;

        //if(Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        //{
        //    Vector3 playerToMouse = floorHit.point - transform.position;
        //    playerToMouse.y = 0f;

        //    Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
        //    playerRigidbody.MoveRotation(newRotation);
        //}

        Vector3 view = new Vector3(0, ch * 1.5f, 0);
        playerTr.Rotate(view * 100 * Time.deltaTime);
    }

    private void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }
}
